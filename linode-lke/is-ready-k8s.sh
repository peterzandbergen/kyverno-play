#!/bin/bash

source $(dirname $0)/include.sh

SETTINGS1=$(dirname $0)/settings.env
SETTINGS2=$(dirname $0)/../settings.env

for settings in $SETTINGS1 $SETTINGS2
do
    if [[ -f $settings ]]
    then
        db_echo Sourcing $settings
        source $settings
    fi
done

if [[ ! -z $1 ]]
then
    CLUSTER_NAME=$1
fi

CLUSTER_ID=$(cluster_id $CLUSTER_NAME)

if [[ -z $CLUSTER_ID ]]
then
    echo Cluster $CLUSTER_NAME not found
    exit 1
fi

echo $(cluster_ready $CLUSTER_ID)

