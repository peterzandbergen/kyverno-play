#!/bin/bash

source $(dirname $0)/include.sh

SETTINGS1=$(dirname $0)/settings.env
SETTINGS2=$(dirname $0)/../settings.env

for settings in $SETTINGS1 $SETTINGS2
do
    if [[ -f $settings ]]
    then
        db_echo Sourcing $settings
        source $settings
    fi
done

if [[ ! -z $1 ]]
then
    CLUSTER_NAME=$1
fi

db_echo Creating cluster $CLUSTER_NAME

# Execute command
linode-cli lke cluster-create \
    --label ${CLUSTER_NAME} \
    --k8s_version=${K8S_VERSION} \
    --node_pools.count=${NODES} \
    --node_pools.type=${POOL_TYPE} \
    --region=${REGION} 