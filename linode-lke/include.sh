# $1 is the cluster name or label
function cluster_id () {
    if [[ -z $1 ]]
    then
        echo ""
        return 1
    fi
    local EXP=".[] | select(.label==\"${1}\").id"
    local CLUSTER_ID=$(\
        linode-cli lke clusters-list --json | \
        jq "$EXP")
    echo $CLUSTER_ID
}

# $1 is the cluster id
function cluster_ready () {
    if [[ -z $1 ]]
    then
        echo ""
        return 1
    fi
    local ready=$(linode-cli lke pools-list $1 --json | \
            jq '. as $all | $all | length as $b | $all | map(select(.nodes.status=="ready")) | length as $a | $a==$b')
    echo $ready
}

function db_echo () {
    if [[ ! -z $DEBUG ]]
    then
        echo $*
    fi
}